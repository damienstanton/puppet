#PowerShell DSC Module for Puppet
---

##Overview

`fireps` allows one to execute PowerShell from Puppet, based on excellent work by Josh Cooper

###Module Description

Where Cooper's `exec` type provides execution of command, this module expands a bit on the`powershell` (renamed `pshell`) using the `fireps` type, and as such, supports all of the `exec` parameters like `creates`, `onlyif`, `unless`, etc. and adds Desired State Configuration, a powerful feature in PowerShell 4.0+


###Usage

To deploy the `WindowsFeature IIS` role:

    fireps { 'Deploy-IIS':
      command   => 'Configuration ContosoWebsite 
	{ 
		 param ($MachineName)

			Node $MachineName 
		{ 
				WindowsFeature IIS 
			{ 
				Ensure = “Present” 
				Name = “Web-Server” 
			} 
				WindowsFeature ASP 
			{ 
				Ensure = “Present” 
				Name = “Web-Asp-Net45” 
			} 
		} 
	}',
      provider  => pshell
    }


###Limitations

 * CAUTION: This is an experimental module and carries the same limitations as Cooper's original.
 * This module requires PowerShell to be installed and the `powershell.exe` to be available in the system `PATH`.
 * Be careful when using PowerShell variables, e.g. `$_`, as they must be escaped in puppet manifests either using backslashes or single quotes.

###License

[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

`
λD
`
